<div class="products">
    <div class="my-container">
        <div class="row">
            <div class="col">
                <h2 class="title">our featured products</h2>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="slider products-slider">
                    <!--- foreach product in products --->
                    <div class="slide">
                        <div class="content">
                            <img src="{{ asset('assets/products/shirt.png') }}" alt="">
                            <div class="description">
                                <p class="title">bruun & stengade <br>nicky - skjorter</p>
                                <p class="pricetag">599,00 DDK</p>
                                <a href="#">buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="slide">
                        <div class="content">
                            <img src="{{ asset('assets/products/shirt2.png') }}" alt="">
                            <div class="description">
                                <p class="title">bruun & stengade <br>nicky - skjorter</p>
                                <p class="pricetag">599,00 DDK</p>
                                <a href="#">buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="slide">
                        <div class="content">
                            <img src="{{ asset('assets/products/shirt3.png') }}" alt="">
                            <div class="description">
                                <p class="title">bruun & stengade <br>nicky - skjorter</p>
                                <p class="pricetag">599,00 DDK</p>
                                <a href="#">buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="slide">
                        <div class="content">
                            <img src="{{ asset('assets/products/shirt4.png') }}" alt="">
                            <div class="description">
                                <p class="title">bruun & stengade <br>nicky - skjorter</p>
                                <p class="pricetag">599,00 DDK</p>
                                <a href="#">buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="slide">
                        <div class="content">
                            <img src="{{ asset('assets/products/shirt3.png') }}" alt="">
                            <div class="description">
                                <p class="title">bruun & stengade <br>nicky - skjorter</p>
                                <p class="pricetag">599,00 DDK</p>
                                <a href="#">buy now</a>
                            </div>
                        </div>
                    </div>
                    <div class="slide">
                        <div class="content">
                            <img src="{{ asset('assets/products/shirt2.png') }}" alt="">
                            <div class="description">
                                <p class="title">bruun & stengade <br>nicky - skjorter</p>
                                <p class="pricetag">599,00 DDK</p>
                                <a href="#">buy now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $(document).ready(function(){
            $('.products-slider').slick({
                autoplay: true,
                autoplaySpeed: 7000,
                slidesToShow: 4,
                centerPadding:'50px',
                dots: true,
                //arrows: true,
                //prevArrow: '<button type="button" data-role="none" class="slick-prev">Previous</button>',
                //nextArrow: '<button type="button" data-role="none" class="slick-next">Next</button>',
                responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2
                    }
                }]
            });
        });
    </script>
@endpush