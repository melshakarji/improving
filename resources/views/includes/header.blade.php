<div class="header">
    <div class="my-container">
        <div class="row">
            <div class="col-md-4 logo-wrapper">
                <span class="logo">improving</span> <span>store</span>
            </div>
            <div class="col-md-5 search">
                <input type="text" placeholder="Indtast søgeord">
            </div>
            <div class="col basket">
                <span>0,00 DDK</span> <img class="icon" src="{{ asset('/assets/basket_dark.png') }}" alt="">
                <div class="basket-content">
                    <div class="row">
                        <div class="col">
                            <p class="uppercase">1 varer</p>
                        </div>
                        <div class="col">
                            <p class="uppercase text-right">Kurv total: <br><b>1.300,00 DDK</b></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <a href="" class="btn btn-primary btn-block">gå til checkout</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col product-list">
                            <div class="list-wrap">
                                <div class="row">
                                    <div class="col-4">
                                        <img src="{{ asset('assets/products/shirt2.png') }}" class="img-fluid" alt="">
                                    </div>
                                    <div class="col">
                                        <p class="title uppercase">bruun & stengade <br>nicky - skjorter</p>
                                        <p class="pricetag">599,00 DDK</p>
                                        <p class="qty">Antal: 1</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col link">
                            <a href="">Se eller rediger din kurv</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>