<div class="footer">
    <div class="my-container">
        <div class="row">
            <div class="col-md-4">
                <p class="heading">lipsumresomen</p>
                <p>Cras a enim accumsan ante pulvinar porttitor. Aliquam
                    pharetra fringilla magna vel condimentum. Mauris rhoncus
                    elit rhoncus sodales luctus. Vestibulum orci urna,
                    malesuada eget nunc faucibus, ultrices pharetra arcu.
                    Suspendisse vulputate libero non sapien tempus rutrum.
                    Nulla lacus velit, convallis quis augue sed, pulvinar faucibus
                    dolor. </p>
            </div>
            <div class="col-md">
                <p class="heading">losmen</p>
                <ul class="unstyled">
                    <li><p>Losmensem</p></li>
                    <li><p>Lamsenm elsmawn</p></li>
                    <li><p>Olemsen damens</p></li>
                    <li><p>Maensen</p></li>
                    <li><p>Posmense</p></li>
                </ul>
            </div>
            <div class="col-md">
                <p class="heading">olsmanes</p>
                <ul class="unstyled">
                    <li><p>Mode</p></li>
                    <li><p>Interiør</p></li>
                    <li><p>Fødevarer</p></li>
                    <li><p>Konferencer</p></li>
                </ul>
            </div>
            <div class="col-md">
                <p class="heading">Kontakt</p>
                <ul class="unstyled">
                    <li><p>Nyhedsbrev</p></li>
                    <li><p>Handelsbetingelser</p></li>
                    <li><p>Sitemap</p></li>
                </ul>
            </div>
            <div class="col-md">
                <ul class="unstyled">
                    <li>
                        <img src="{{ asset('assets/socials/facebook.png') }}" alt="">
                    </li>
                    <li>
                        <img src="{{ asset('assets/socials/instagram.png') }}" alt="">
                    </li>
                    <li>
                        <img src="{{ asset('assets/socials/linkedin.png') }}" alt="">
                    </li>
                    <li>
                        <img src="{{ asset('assets/socials/twitter.png') }}" alt="">
                    </li>
                    <li>
                        <img src="{{ asset('assets/socials/pinterest.png') }}" alt="">
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>