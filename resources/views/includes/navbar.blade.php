<div class="section-navigation">
    <div class="my-container">
        <div class="row top-nav">
            <div class="col">
                <ul class="unstyled">
                    <li class="sub-nav"><a href="#">woman</a><div class="sub-nav-content">
                <ul>
                    <li><a href="">sunglasses</a></li>
                    <li><a href="">shirts & tops</a></li>
                    <li><a href="">designer tops</a></li>
                    <li><a href="">blouses</a></li>
                    <li><a href="">socks & tights</a></li>
                    <li><a href="">groing out</a></li>
                </ul>
            </div></li>
                    <li><a href="#">men</a></li>
                    <li><a href="#">junior</a></li>
                    <li><a href="#">accessories</a></li>
                    <li><a href="#">collections</a></li>
                </ul>
            </div>
            
        </div>
        <div class="row mobile-nav">
            <div class="col-4">
                <img onclick="openMobileNav()" src="{{ asset('assets/nav-btn.png') }}" alt="">
            </div>
            <div class="col-4 text-center">
                <img src="{{ asset('assets/search.png') }}" alt="">
            </div>
            <div class="col-4 text-right">
                <img src="{{ asset('assets/basket_light.png') }}" alt="">
            </div>
        </div>
    </div>
    <div id="mobile-nav-content">
        <div class="row">
            <div class="col">
                <p id="nav-title">Menu</p>
            </div>
            <div class="col">
                <img onclick="closeMobileNav()" src="{{ asset('assets/close.png') }}" alt="">
            </div>
        </div>
        <div class="row">
            <div class="col">
                <ul class="unstyled">
                    <li><a href="#" onclick="onward('woman')">woman</a></li>
                    <li><a href="#" onclick="onward('men')">men</a></li>
                    <li><a href="#" onclick="onward('junior')">junior</a></li>
                    <li><a href="#" onclick="onward('accessories')">accessories</a></li>
                    <li><a href="#" onclick="onward('collections')">collections</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>


@push('scripts')
    <script>
        function openMobileNav() {
            $('#mobile-nav-content').addClass('open');
        }
        function closeMobileNav() {
            $('#mobile-nav-content').removeClass('open');
        }

        function onward(data) {
            $('#nav-title').text(data);
            // function to change out main nav points with sub nav
        }
    </script>
@endpush